# ColorSep
A utility for separating a single png (such as one produced by an [inkscape](https://inkscape.org/) export) into
separate per-color images such as might be useful for silk screen printing at a place like
[ooshirts](http://www.ooshirts.com).

## Features
The current feature set includes:
- separation into a specifiable number colors
- a "near pixel threshold" option for fuzzily matching colors that are close but not exactly what's expected
- a "white base" option to produce a white pixel under all active pixels of any color
- a specifiable alpha threshold beneath which all pixels will be discarded

## Requirements
Colorsep probably requires python 3.  I'll gladly accept PRs to fix this and/or assertions to the contrary.  I just
haven't tested it.

## Install
It should be as simple as `pip install colorsep` for python-friendly folks.

## Basic usage
To generate the images I needed to make my t-shirt, I ran this:
```colorsep --input resources/farm_scene.png --output-base output/output_ --num-colors=7 --base-white --near-pixel-threshold=100```

To get help:
```
$ colorsep --help
Usage: colorsep [OPTIONS]

Options:
  --input TEXT                    input png
  --output-base TEXT              base name of output pngs
  --alpha-threshold INTEGER       on/off threshold for alpha
  --num-colors TEXT               use to set number of output colors
  --base-white                    should all color be underlaid with white?
  --near-pixel-threshold INTEGER  tolerance for nearest-color computation
  --help                          Show this message and exit.
$
```

